package com.whitaaplikasi.footballclub

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide

class ClubAdapter(private val context : Context, val lists : List<ItemClub>)
    : RecyclerView.Adapter<ClubAdapter.ClubHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ClubHolder(LayoutInflater.from(context)
                    .inflate(R.layout.list_item,parent, false))

    override fun getItemCount(): Int  = lists.size

    override fun onBindViewHolder(holder: ClubHolder, position: Int) {
        holder.bindItem(lists[position])

    }

    class ClubHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        val nama = itemView.findViewById<TextView>(R.id.nama)
        val gambar = itemView.findViewById<ImageView>(R.id.gambar)

        fun bindItem(items : ItemClub){
            nama.text = items.nama_club
            Glide.with(itemView.context).load(items.gambar).into(gambar)
        }

    }
}