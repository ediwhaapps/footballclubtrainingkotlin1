package com.whitaaplikasi.footballclub

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView

class DataClub : AppCompatActivity() {

    var datalists : MutableList<ItemClub> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_club)

        val list =  findViewById<RecyclerView>(R.id.clubView)
        list.layoutManager = LinearLayoutManager(this)
        list.adapter = ClubAdapter(this, datalists)

        initData()


    }

    private fun initData() {
        val nama_club = resources.getStringArray(R.array.nama_club)
        val image = resources.obtainTypedArray(R.array.club_image)
        datalists.clear()
        for (i in nama_club.indices){
            datalists.add(ItemClub(nama_club[i],
                    image.getResourceId(i,0)))
        }

        image.recycle()

    }
}
